﻿namespace Homework_12.Observer_patern
{
    internal interface IObserver
    {
        void Update(double price);
    }
}
