﻿namespace Homework_12.Observer_patern
{
    internal class Housing
    {
        public delegate void PriceDroppedDelegate(double newPrice);
        public event PriceDroppedDelegate PriceDropped;

        private double _price;

        public double Price
        {
            get => _price;
            set
            {
                if (value < 500)
                {
                    _price = value;
                    PriceDropped?.Invoke(_price);
                }
            }
        }

    }
}
