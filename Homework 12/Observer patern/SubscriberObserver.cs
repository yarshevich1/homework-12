﻿namespace Homework_12.Observer_patern
{
    internal class SubscriberObserver : IObserver
    {
        public void Update(double price) => Console.WriteLine($"Price dropped to ${price}");
    }
}
