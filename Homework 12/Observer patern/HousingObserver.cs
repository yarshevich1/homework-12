﻿using System;

namespace Homework_12.Observer_patern
{
    internal class HousingObserver
    {
        List<IObserver> _observers = new List<IObserver>();

        private double _price;

        public double Price
        {
            get => _price;
            set
            {
                if (value < 500)
                {
                    _price = value;
                    NotifyObservers();
                }
            }
        }

        public void AddObserver(IObserver observer) => _observers.Add(observer);

        public void RemoveObserver(IObserver observer) => _observers.Remove(observer);

        private void NotifyObservers()
        {
            foreach (var observer in _observers)
            {
                observer.Update(_price);
            }
        }
    }
}
