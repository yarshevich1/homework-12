﻿namespace Homework_12.Observer_patern
{
    internal class Subscriber
    {
        public void OnPriceDropped(double price) => Console.WriteLine($"Price dropped to ${price}");
    }
}
