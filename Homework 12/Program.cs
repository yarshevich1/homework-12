﻿using Homework_12.Observer_patern;

namespace Homework_12
{
    internal class Program
    {
        static Random rnd = new Random();
        static void Main(string[] args)
        {
            // Task 1
            AveragePriceMonitor priceMonitor = new AveragePriceMonitor(ShowPrice);
            priceMonitor.TrackPrice();

            Console.WriteLine("\r********************************************\r");

            // Task 2 part 1
            Housing housing = new Housing();
            housing.PriceDropped += Housing_PriceDropped;

            for (int i = 0; i < 5; i++)
            {
                double currentPrice = rnd.Next(100, 1000);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Current price is: ${currentPrice}");
                Console.ForegroundColor = ConsoleColor.White;
                housing.Price = currentPrice;
                Thread.Sleep(500);
            }

            // Task 2 part 2
            HousingObserver housingObserver = new HousingObserver();
            SubscriberObserver[] subscriberObserver = new []
            {
                new SubscriberObserver(),
                new SubscriberObserver(),
                new SubscriberObserver(),
                new SubscriberObserver()
            };
            foreach (var observer in subscriberObserver)
            {
                housingObserver.AddObserver(observer);
            }

            for (int i = 0; i < 5; i++)
            {
                double currentPrice = rnd.Next(100, 1000);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Current price is: ${currentPrice}");
                Console.ForegroundColor = ConsoleColor.White;
                housingObserver.Price = currentPrice;
                Thread.Sleep(500);
            }
        }

        private static void Housing_PriceDropped(double newPrice)
        {
            Subscriber subscriber = new Subscriber();
            subscriber.OnPriceDropped(newPrice);
        }

        /// <summary>
        /// Input average price
        /// </summary>
        /// <param name="price">price</param>
        private static void ShowPrice(int price)
        {
            Console.WriteLine($"Average price of the house in this street is ${price}");
            ;
        }
    }
}