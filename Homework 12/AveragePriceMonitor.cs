﻿namespace Homework_12
{
    internal class AveragePriceMonitor
    {
        // Create random class
        Random rnd = new Random();

        // Create action delegate
        private Action<int> ShowPrice;

        /// <summary>
        /// Сonstructor that accepts the delegate
        /// </summary>
        /// <param name="showPrice">Method show price</param>
        public AveragePriceMonitor(Action<int> showPrice)
        {
            ShowPrice = showPrice;
        }

        /// <summary>
        /// Method that call delegate
        /// </summary>
        public void TrackPrice()
        {
            ShowPrice(rnd.Next(100, 1000));
        }
    }
}
